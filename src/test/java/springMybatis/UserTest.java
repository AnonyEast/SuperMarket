package springMybatis;

import mapper.SmbmsUserMapper;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import pojo.SmbmsUser;
import pojo.SmbmsUserExample;

import java.util.List;

public class UserTest {
    //加载Spring配置文件，启动Spring
    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    //从Spring容器获取userMapper对象
    SmbmsUserMapper userMapper = context.getBean(SmbmsUserMapper.class);

    @Test
    public void testUser() {
        //1.查询所有用户信息
        List<SmbmsUser> userList = userMapper.selectByExample(new SmbmsUserExample());
        for (SmbmsUser user : userList) {
            System.out.println(user);
        }

        //2.查询用户总数
        int countUser = userMapper.countByExample(new SmbmsUserExample());
        System.out.println("用户总数:" + countUser);

        //3.删除一个用户
        SmbmsUserExample userDeleteExample = new SmbmsUserExample();
        SmbmsUserExample.Criteria criteria = userDeleteExample.createCriteria();
        criteria.andIdEqualTo(16L);
        int countDeleted = userMapper.deleteByExample(userDeleteExample);
        System.out.println("删除了" + countDeleted + "个用户");

        //4.添加一个用户
        SmbmsUser user = new SmbmsUser();
        user.setId(20L);
        user.setUsercode("anonyeast");
        user.setUsername("ydd");
        int countInsert = userMapper.insert(user);
        System.out.println("添加了" + countInsert + "条记录");
    }

    @Test
    public void testLogin() {
        SmbmsUser loginUser = new SmbmsUser();
        loginUser.setUsercode("admin");
        loginUser.setUserpassword("1");
        SmbmsUserExample example = new SmbmsUserExample();
        SmbmsUserExample.Criteria criteria = example.createCriteria();
        criteria.andUsercodeEqualTo(loginUser.getUsercode());
        criteria.andUserpasswordEqualTo(loginUser.getUserpassword());
        List<SmbmsUser> userList = userMapper.selectByExample(example);
        for (SmbmsUser user : userList) {
            System.out.println(user);
        }
    }

    @Test
    public void userCodeIsExist() {
        SmbmsUserExample example = new SmbmsUserExample();
        SmbmsUserExample.Criteria criteria = example.createCriteria();
        criteria.andUsercodeEqualTo("bc");
        List<SmbmsUser> userList = userMapper.selectByExample(example);
        if (userList.isEmpty()) {
            System.out.println("可使用");
        } else {
            System.out.println("已被使用");
        }
    }

    @Test
    public void modifyPassword() {
        SmbmsUser user = new SmbmsUser();
        user.setId(1L);
        user.setUsercode("admin");
        user.setUserpassword("1");
        String newPassword = "admin";
        SmbmsUserExample example = new SmbmsUserExample();
        SmbmsUserExample.Criteria criteria = example.createCriteria();
        criteria.andUsercodeEqualTo(user.getUsercode());
        user.setUserpassword(newPassword);
        int count = userMapper.updateByExample(user, example);
        System.out.println("更新了" + count + "条记录");
    }
}
