package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pojo.SmbmsBill;
import pojo.SmbmsProvider;
import services.BillServices;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class BillController {
    @Autowired
    BillServices billServices;

    /**
     * 进入订单管理页面
     * @param session
     * @return
     */
    @RequestMapping("/sys/bill")
    public String displayBillPage(HttpSession session){
        List<SmbmsBill> billList = billServices.getAllBill();
        List<SmbmsProvider> providerList = billServices.getAllProvider();
        session.setAttribute("providerList",providerList);
        session.setAttribute("billList",billList);
        return "/bill/billlist";
    }

    /**
     * 订单搜索
     * @param bill
     * @param model
     * @return
     */
    @RequestMapping("/sys/billSearch")
    public String billSearch(SmbmsBill bill,Model model){
        List<SmbmsBill> billBySearch = billServices.getBillBySearch(bill);
        model.addAttribute("billList",billBySearch);
        return "/bill/billlist";
    }

    /**
     * 进入添加订单页面
     * @param model
     * @return
     */
    @RequestMapping("/sys/billadd")
    public String displayBillAddPage(Model model){
        List<SmbmsProvider> providerList = billServices.getAllProvider();
        model.addAttribute("providerList",providerList);
        return "/bill/billadd";
    }

    /**
     * 添加订单
     * @param bill
     * @param model
     * @return
     */
    @RequestMapping("/sys/dobilladd")
    public String addBill(SmbmsBill bill,Model model){
        int count = billServices.addBill(bill);
        if (count>0){
            model.addAttribute("bill_msg","订单创建成功");
        }else {
            model.addAttribute("bill_msg","订单创建失败");
        }
        return "/bill/billadd";
    }

    /**
     * 进入订单查看页面
     * @param model
     * @return
     */
    @RequestMapping("/sys/billview")
    public String viewBill(Model model){

        return "/bill/billview";
    }
}
