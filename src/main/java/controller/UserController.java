package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pojo.SmbmsRole;
import pojo.SmbmsUser;
import services.UserRoleServices;
import services.UserServices;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

@Controller
public class UserController {
    @Autowired //用于从Spring容器中取出对象(通过名称或类型获取)
    UserServices userServices;
    @Autowired
    UserRoleServices roleServices;

    /**
     * 登录
     *
     * @param loginUser
     * @param session
     * @param model
     * @return
     */
    @RequestMapping("/dologin")
    public String login(SmbmsUser loginUser, HttpSession session, Model model) {
        SmbmsUser user = userServices.getUserByLogin(loginUser);
        if (user == null) {
            model.addAttribute("error", "很高兴，你输入的用户名或密码错误");
            return "login";
        }
        session.setAttribute("userOnLogin", user);
        return "frame";
    }

    /**
     * 退出登录
     *
     * @param session
     * @param model
     * @return
     */
    @RequestMapping("/sys/logout")
    public String logout(HttpSession session, Model model) {
        session.removeAttribute("userOnLogin");
        model.addAttribute("error", "您已退出登录");
        return "login";
    }

    /**
     * 显示用户列表
     *
     * @param model
     * @return
     */
    @RequestMapping("/sys/user")
    public String getAllUser(Model model, HttpSession session) {
        List<SmbmsUser> userList = userServices.getAllUser();
        model.addAttribute("userList", userList);
        List<SmbmsRole> roleList = roleServices.getAllRole();
        session.setAttribute("roleList", roleList);
        return "/user/userlist";
    }

    /**
     * 显示用户添加页面
     *
     * @return
     */
    @RequestMapping("/sys/useradd")
    public String displayAddUserPage(Model model) {
        List<SmbmsRole> roleList = roleServices.getAllRole();
        model.addAttribute("roleList", roleList);
        return "/user/useradd";
    }

    /**
     * 添加用户
     *
     * @param user
     * @return
     */
    @RequestMapping("/sys/saveuser")
    public String addUser(SmbmsUser user, Model model) {
        int count = userServices.addUser(user);
        if (count > 0) {
            model.addAttribute("reg_msg", "用户添加成功");
        } else {
            model.addAttribute("reg_msg", "用户添加失败");
        }
        return "/user/useradd";
    }

    /**
     * Ajax判断用户名是否被占用
     *
     * @param response
     * @param userCode
     */
    @RequestMapping("/sys/usercode")
    public void userCodeIsExist(HttpServletResponse response, String userCode) {
        try {
            response.setContentType("application/json;charset=utf-8");
            boolean isExist = userServices.userCodeIsExist(userCode);
            HashMap<String, Object> map = new HashMap<>();
            map.put("userExist", isExist);
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(response.getWriter(), map);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 搜索用户
     *
     * @param user
     * @param model
     * @return
     */
    @RequestMapping("/sys/userSearch")
    public String searchUser(SmbmsUser user, Model model) {
        List<SmbmsUser> userBySearch = userServices.getUserBySearch(user);
        model.addAttribute("userList", userBySearch);
        return "/user/userlist";
    }

    /**
     * 进入修改密码页面
     *
     * @return
     */
    @RequestMapping("/sys/pwdmodify")
    public String displayModifyPasswordPage() {
        return "/user/pwdmodify";
    }

    /**
     * 修改密码
     *
     * @param model
     * @param oldPassword
     * @param newPassword
     * @param session
     * @return
     */
    @RequestMapping("/sys/savepwdmodify")
    public String modifyPassword(Model model, @RequestParam("oldpassword") String oldPassword,
                                 @RequestParam("newpassword") String newPassword, HttpSession session) {
        SmbmsUser user = (SmbmsUser) session.getAttribute("userOnLogin");
        if (!user.getUserpassword().equals(oldPassword)) {
            model.addAttribute("message", "原密码错误");
            return "/user/pwdmodify";
        }
        int count = userServices.modifyPassword(user, newPassword);
        if (count > 0) {
            model.addAttribute("message", "密码修改成功");
        }
        return "/user/pwdmodify";
    }
}
