package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pojo.SmbmsProvider;
import pojo.SmbmsUser;
import services.ProviderServices;

import java.util.List;

/**
 * 供应商管理前端控制器
 */
@Controller
public class ProviderController {
    @Autowired
    ProviderServices providerServices;

    /**
     * 获取所有供应商
     * @param model
     * @return
     */
    @RequestMapping("/sys/provider")
    public String getAllProviderr(Model model){
        List<SmbmsProvider> allProvider = providerServices.getAllProvider();
        model.addAttribute("proList",allProvider);
        return "/pro/providerlist";
    }

    /**
     * 供应商搜索
     * @param provider
     * @param model
     * @return
     */
    @RequestMapping("/sys/providerSearch")
    public String searchProvider(SmbmsProvider provider, Model model) {
        List<SmbmsProvider> providerBySearch = providerServices.getProviderBySearch(provider);
        model.addAttribute("proList", providerBySearch);
        return "/pro/providerlist";
    }

    /**
     * 进入供应商添加页面
     * @return
     */
    @RequestMapping("/sys/provideradd")
    public String displayProviderAddPage(){
        return "/pro/provideradd";
    }

    @RequestMapping("/sys/provideraddsave")
    public String addProvider(SmbmsProvider provider,Model model){
        int count = providerServices.addProvider(provider);
        if (count > 0){
            model.addAttribute("provider_msg","供应商创建成功");
        }else{
            model.addAttribute("provider_msg","供应商创建失败");
        }
        return "/pro/provideradd";
    }
}
