package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

//只有在springmvc配置文件中开启了自动扫描，注解才生效
@Controller //声明这个类是一个控制器
public class PageController {
    @RequestMapping("/index")
    public String index(){
        return "login";//视图解析器:WEB-INF/jsp/login.jsp
    }
}
