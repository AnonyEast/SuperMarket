package services;

import mapper.SmbmsRoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pojo.SmbmsRole;

import java.util.List;

/**
 * 用户角色模块
 */
@Service
public class UserRoleServices {
    @Autowired
    SmbmsRoleMapper roleMapper;

    public List<SmbmsRole> getAllRole(){
        return roleMapper.selectByExample(null);
    }
}
