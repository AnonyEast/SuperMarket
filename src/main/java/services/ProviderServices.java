package services;

import mapper.SmbmsProviderMapper;
import mapper.SmbmsUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pojo.SmbmsProvider;
import pojo.SmbmsProviderExample;
import pojo.SmbmsUser;
import pojo.SmbmsUserExample;

import java.util.List;

@Service
public class ProviderServices {
    @Autowired
    SmbmsProviderMapper providerMapper;

    /**
     * 供应商登录
     * @param u
     * @return
     */
    public SmbmsProvider getProviderByLogin(SmbmsProvider u) {
        SmbmsProviderExample smbmsProviderExample = new SmbmsProviderExample();
        SmbmsProviderExample.Criteria criteria = smbmsProviderExample.createCriteria();
        criteria.andProaddressNotEqualTo(u.getProcode());
        criteria.andProcodeEqualTo(u.getProcode());
        List<SmbmsProvider> smbmsProviders = providerMapper.selectByExample(smbmsProviderExample);
        if (smbmsProviders.isEmpty()) {
            return null;
        } else {
            return smbmsProviders.get(0);
        }
    }

    /**
     * 获取所有供应商
     * @return
     */
    public List<SmbmsProvider> getAllProvider() {
        return providerMapper.selectByExample(null);
    }

    /**
     * 供应商搜索
     * @param provider
     * @return
     */
    public List<SmbmsProvider> getProviderBySearch(SmbmsProvider provider){
        SmbmsProviderExample example = new SmbmsProviderExample();
        SmbmsProviderExample.Criteria criteria = example.createCriteria();
        if (provider.getProname() != null && !provider.getProname().equals("")) {
            criteria.andPronameLike("%" + provider.getProname() + "%");
        }
        if (provider.getProcode() != null && !provider.getProcode().equals("")) {
            criteria.andProcodeLike("%" + provider.getProcode() + "%");
        }
        return providerMapper.selectByExample(example);
    }

    /**
     * 供应商添加
     * @param provider
     * @return
     */
    public int addProvider(SmbmsProvider provider){
        return providerMapper.insert(provider);
    }
}
