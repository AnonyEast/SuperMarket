package services;

import mapper.SmbmsBillMapper;
import mapper.SmbmsProviderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pojo.SmbmsBill;
import pojo.SmbmsBillExample;
import pojo.SmbmsProvider;

import java.util.List;

@Service
public class BillServices {
    @Autowired
    SmbmsBillMapper billMapper;
    @Autowired
    SmbmsProviderMapper providerMapper;

    /**
     * 获取所有订单
     *
     * @return
     */
    public List<SmbmsBill> getAllBill() {
        return billMapper.selectByExample(null);
    }

    /**
     * 获取所有供应商
     *
     * @return
     */
    public List<SmbmsProvider> getAllProvider() {
        return providerMapper.selectByExample(null);
    }

    /**
     * 搜索订单
     *
     * @return
     */
    public List<SmbmsBill> getBillBySearch(SmbmsBill bill) {
        SmbmsBillExample example = new SmbmsBillExample();
        SmbmsBillExample.Criteria criteria = example.createCriteria();
        if (bill.getProductname() != null && !bill.getProductname().equals("")) {
            criteria.andProductnameLike("%" + bill.getProductname() + "%");
        }
        if (bill.getProviderid() != null && bill.getProviderid() != 0) {
            criteria.andProvideridEqualTo(bill.getProviderid());
        }
        if (bill.getIspayment() != 0 && bill.getProviderid() != null) {
            criteria.andIspaymentEqualTo(bill.getIspayment());
        }
        return billMapper.selectByExample(example);
    }

    /**
     * 添加订单
     * @param bill
     * @return
     */
    public int addBill(SmbmsBill bill){
        int count = billMapper.insert(bill);
        return count;
    }
}
